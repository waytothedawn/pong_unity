﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    public float velocityY;
    private float posY;
    public float maxY;
    public float minY;
    private float direction;

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        direction = Input.GetAxis("Vertical");
        posY = transform.position.y + direction*velocityY*Time.deltaTime;

          if(posY > maxY){
            posY = maxY;
        }
          if(posY < minY){
           posY = minY;
        }

        transform.position = new Vector3(transform.position.x, posY, transform.position.z);

        
      
    }
}
